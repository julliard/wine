/*
 * WoW64 support using box86
 *
 * Copyright 2023 Alexandre Julliard
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdarg.h>
#include <stdint.h>
#include <signal.h>

#include "ntstatus.h"
#define WIN32_NO_STATUS
#include "windef.h"
#include "winnt.h"
#include "winternl.h"
#include "wine/asm.h"
#include "wine/debug.h"
#include "wine/unixlib.h"
#include "x86emu.h"
#include "x86run.h"
#include "x86trace.h"
#include "box86context.h"
#include "emu/x86emu_private.h"
#include "emu/x87emu_private.h"

WINE_DEFAULT_DEBUG_CHANNEL(wow);

#include "pshpack1.h"
struct thunk_32to64
{
    WORD  int2e;
};
struct thunk_opcodes
{
    struct thunk_32to64 syscall_thunk;    /* int $0x2e */
    struct
    {
        BYTE  movl;  /* movl $magic,%eax */
        DWORD magic;
        struct thunk_32to64 t;
    } unix_thunk;
};
#define UNIX_CALL_MAGIC_ID 0xfa57ca11
#include "poppack.h"

extern NTSTATUS WINAPI Wow64SystemServiceEx( UINT num, UINT *args );

static BYTE DECLSPEC_ALIGN(4096) code_buffer[0x1000];

static uint32_t x86emu_parity_tab[8] =
{
	0x96696996,
	0x69969669,
	0x69969669,
	0x96696996,
	0x69969669,
	0x96696996,
	0x96696996,
	0x69969669,
};

void UnimpOpcode(x86emu_t* emu)
{
    emu->quit=1;
    emu->error |= ERR_UNIMPL;
}

int my_getcontext(x86emu_t* emu, void* ucp)
{
    return 0;
}

int my_setcontext(x86emu_t* emu, void* ucp)
{
    return R_EAX;
}

void emit_signal( x86emu_t *emu, int sig, void *addr, int code )
{
    EXCEPTION_RECORD rec;

    ERR( "got signal %u\n", sig );
    switch (sig)
    {
    case SIGILL:
        rec.ExceptionCode = STATUS_ILLEGAL_INSTRUCTION;
        break;
    case SIGSEGV:
        rec.ExceptionCode = STATUS_ACCESS_VIOLATION;
        break;
    default:
        ERR( "unknown signal %u\n", sig );
        rec.ExceptionCode = STATUS_ACCESS_VIOLATION;
        break;
    }
    rec.ExceptionFlags   = EH_NONCONTINUABLE;
    rec.ExceptionRecord  = NULL;
    rec.ExceptionAddress = addr;
    rec.NumberParameters = 0;
    RtlRaiseException( &rec );
}

void x86Int3( x86emu_t *emu )
{
    EXCEPTION_RECORD rec;

    ERR( "got int3 at %x\n", R_EIP );
    rec.ExceptionCode    = STATUS_BREAKPOINT;
    rec.ExceptionFlags   = 0;
    rec.ExceptionRecord  = NULL;
    rec.ExceptionAddress = (void *)(uintptr_t)R_EIP;
    rec.NumberParameters = 0;
    RtlRaiseException( &rec );
}

void x86Int(x86emu_t *emu, int code)
{
    if (code == 0x2e)  /* NT syscall */
    {
        WOW64_CPURESERVED *cpu = NtCurrentTeb()->TlsSlots[WOW64_TLS_CPURESERVED];
        I386_CONTEXT *ctx = (I386_CONTEXT *)(cpu + 1);
        int id = R_EAX;

        R_EIP = Pop(emu);
        ctx->Eip = R_EIP;
        ctx->Esp = R_ESP;
        ctx->Ebx = R_EBX;
        ctx->Esi = R_ESI;
        ctx->Edi = R_EDI;
        ctx->Ebp = R_EBP;
        ctx->EFlags = emu->eflags.x32;
        cpu->Flags = 0;

        if (id == UNIX_CALL_MAGIC_ID)
        {
            uintptr_t handle_low = Pop(emu);
            uintptr_t handle_high = Pop(emu);
            unsigned int code = Pop(emu);
            uintptr_t args = Pop(emu);
            ctx->Esp = R_ESP;
            ERR("unix call\n");
            R_EAX = __wine_unix_call_dispatcher( handle_low | (handle_high << 32), code, (void *)args );
        }
        else
        {
            R_EAX = Wow64SystemServiceEx( id, (UINT *)(uintptr_t)R_ESP + 1 );
        }
        R_EBX = ctx->Ebx;
        R_ESI = ctx->Esi;
        R_EDI = ctx->Edi;
        R_EBP = ctx->Ebp;
        R_ESP = ctx->Esp;
        R_EIP = ctx->Eip;
        if (cpu->Flags & WOW64_CPURESERVED_FLAG_RESET_STATE)
        {
            cpu->Flags &= ~WOW64_CPURESERVED_FLAG_RESET_STATE;
            R_EAX = ctx->Eax;
            R_ECX = ctx->Ecx;
            R_EDX = ctx->Edx;
            R_FS  = ctx->SegFs;
            emu->eflags.x32 = ctx->EFlags;
        }
    }
    else
    {
        ERR( "%x not supported\n", code );
        RtlRaiseStatus( STATUS_ACCESS_VIOLATION );
    }
}

int GetTID(void)
{
    return GetCurrentThreadId();
}

uint64_t ReadTSC( x86emu_t *emu )
{
    LARGE_INTEGER counter;
    NtQueryPerformanceCounter( &counter, NULL );
    return counter.QuadPart;  /* FIXME */
}

void my_cpuid( x86emu_t *emu, uint32_t code )
{
    ERR( "%x: stub\n", code );
}

void *GetSegmentBase( uint32_t desc )
{
    FIXME( "%x: stub\n", desc );
    return NULL;
}

uint32_t default_fs = 0; /* FIXME */

static box86context_t box86_context;

/**********************************************************************
 *           BTCpuSimulate  (box86cpu.@)
 */
void WINAPI BTCpuSimulate(void)
{
    x86emu_t *emu = NtCurrentTeb()->TlsSlots[0];  // FIXME
    WOW64_CPURESERVED *cpu = NtCurrentTeb()->TlsSlots[WOW64_TLS_CPURESERVED];
    I386_CONTEXT *ctx = (I386_CONTEXT *)(cpu + 1);
    XMM_SAVE_AREA32 *fpu = (XMM_SAVE_AREA32 *)ctx->ExtendedRegisters;

    R_EAX = ctx->Eax;
    R_EBX = ctx->Ebx;
    R_ECX = ctx->Ecx;
    R_EDX = ctx->Edx;
    R_ESI = ctx->Esi;
    R_EDI = ctx->Edi;
    R_EBP = ctx->Ebp;
    R_EIP = ctx->Eip;
    R_ESP = ctx->Esp;
    R_CS  = ctx->SegCs;
    R_DS  = ctx->SegDs;
    R_ES  = ctx->SegEs;
    R_FS  = ctx->SegFs;
    R_GS  = ctx->SegGs;
    R_SS  = ctx->SegSs;
    emu->eflags.x32 = ctx->EFlags;
    emu->segs_offs[_FS] = (uintptr_t)NtCurrentTeb() + NtCurrentTeb()->WowTebOffset;

    reset_fpu(emu);
    emu->mxcsr = fpu->MxCsr;
    Run(emu, 0);
    ERR("finished eip %x stack %x\n", R_EIP, R_ESP);
}

/**********************************************************************
 *           BTCpuProcessInit  (box86cpu.@)
 */
NTSTATUS WINAPI BTCpuProcessInit(void)
{
    struct thunk_opcodes *thunk = (struct thunk_opcodes *)code_buffer;
    SIZE_T size = sizeof(*thunk);
    ULONG old_prot;
    HMODULE module;
    UNICODE_STRING str;
    void **p__wine_unix_call_dispatcher;

    if ((ULONG_PTR)code_buffer >> 32)
    {
        ERR( "box86cpu loaded above 4G, disabling\n" );
        return STATUS_INVALID_ADDRESS;
    }

    RtlInitUnicodeString( &str, L"ntdll.dll" );
    LdrGetDllHandle( NULL, 0, &str, &module );
    p__wine_unix_call_dispatcher = RtlFindExportedRoutineByName( module, "__wine_unix_call_dispatcher" );
    __wine_unix_call_dispatcher = *p__wine_unix_call_dispatcher;

    thunk->syscall_thunk.int2e = 0x2ecd;
    thunk->unix_thunk.movl     = 0xb8;
    thunk->unix_thunk.magic    = UNIX_CALL_MAGIC_ID;
    thunk->unix_thunk.t.int2e  = 0x2ecd;

    NtProtectVirtualMemory( GetCurrentProcess(), (void **)&thunk, &size, PAGE_EXECUTE_READ, &old_prot );

    RtlInitializeCriticalSection(&box86_context.mutex_once);
    RtlInitializeCriticalSection(&box86_context.mutex_once2);
    RtlInitializeCriticalSection(&box86_context.mutex_trace);
#ifdef DYNAREC
    RtlInitializeCriticalSection(&box86_context.mutex_dyndump);
#else
    RtlInitializeCriticalSection(&box86_context.mutex_lock);
#endif
    RtlInitializeCriticalSection(&box86_context.mutex_tls);
    RtlInitializeCriticalSection(&box86_context.mutex_thread);
    RtlInitializeCriticalSection(&box86_context.mutex_bridge);
    InitX86Trace(&box86_context);

    return STATUS_SUCCESS;
}


/**********************************************************************
 *           BTCpuThreadInit  (box86cpu.@)
 */
NTSTATUS WINAPI BTCpuThreadInit(void)
{
    I386_CONTEXT *ctx;
    x86emu_t *emu = RtlAllocateHeap( GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(*emu) );

    RtlWow64GetCurrentCpuArea( NULL, (void **)&ctx, NULL );
    emu->context = &box86_context;

    // setup cpu helpers
    for (int i=0; i<8; ++i)
        emu->sbiidx[i] = &emu->regs[i];
    emu->sbiidx[4] = &emu->zero;
    emu->x86emu_parity_tab = x86emu_parity_tab;

    NtCurrentTeb()->TlsSlots[0] = emu;  // FIXME
    return STATUS_SUCCESS;
}


/**********************************************************************
 *           BTCpuGetBopCode  (box86cpu.@)
 */
void * WINAPI BTCpuGetBopCode(void)
{
    struct thunk_opcodes *thunk = (struct thunk_opcodes *)code_buffer;

    return &thunk->syscall_thunk;
}


/**********************************************************************
 *           __wine_get_unix_opcode  (box86cpu.@)
 */
void * WINAPI __wine_get_unix_opcode(void)
{
    struct thunk_opcodes *thunk = (struct thunk_opcodes *)code_buffer;

    return &thunk->unix_thunk;
}


/**********************************************************************
 *           BTCpuGetContext  (box86cpu.@)
 */
NTSTATUS WINAPI BTCpuGetContext( HANDLE thread, HANDLE process, void *unknown, I386_CONTEXT *ctx )
{
    x86emu_t *emu = NtCurrentTeb()->TlsSlots[0];
    DWORD flags = ctx->ContextFlags & ~CONTEXT_i386;

    if (flags & CONTEXT_I386_CONTROL)
    {
        ctx->Ebp    = R_EBP;
        ctx->Esp    = R_ESP;
        ctx->Eip    = R_EIP;
        ctx->SegCs  = R_CS;
        ctx->SegSs  = R_SS;
        ctx->EFlags = emu->eflags.x32;
    }
    if (flags & CONTEXT_I386_INTEGER)
    {
        ctx->Eax = R_EAX;
        ctx->Ebx = R_EBX;
        ctx->Ecx = R_ECX;
        ctx->Edx = R_EDX;
        ctx->Esi = R_ESI;
        ctx->Edi = R_EDI;
    }
    if (flags & CONTEXT_I386_SEGMENTS)
    {
        ctx->SegDs = R_DS;
        ctx->SegEs = R_ES;
        ctx->SegFs = R_FS;
        ctx->SegGs = R_GS;
    }
#if 0  /* FIXME */
    if (flags & CONTEXT_I386_FLOATING_POINT)
    {
        ctx->FloatSave.ControlWord   = from->fp.i386_regs.ctrl;
        ctx->FloatSave.StatusWord    = from->fp.i386_regs.status;
        ctx->FloatSave.TagWord       = from->fp.i386_regs.tag;
        ctx->FloatSave.ErrorOffset   = from->fp.i386_regs.err_off;
        ctx->FloatSave.ErrorSelector = from->fp.i386_regs.err_sel;
        ctx->FloatSave.DataOffset    = from->fp.i386_regs.data_off;
        ctx->FloatSave.DataSelector  = from->fp.i386_regs.data_sel;
        ctx->FloatSave.Cr0NpxState   = from->fp.i386_regs.cr0npx;
        memcpy( ctx->FloatSave.RegisterArea, from->fp.i386_regs.regs, sizeof(ctx->FloatSave.RegisterArea) );
    }
    if ((from->flags & SERVER_CTX_DEBUG_REGISTERS) && (to_flags & CONTEXT_I386_DEBUG_REGISTERS))
    {
        ctx->ContextFlags |= CONTEXT_I386_DEBUG_REGISTERS;
        ctx->Dr0 = from->debug.i386_regs.dr0;
        ctx->Dr1 = from->debug.i386_regs.dr1;
        ctx->Dr2 = from->debug.i386_regs.dr2;
        ctx->Dr3 = from->debug.i386_regs.dr3;
        ctx->Dr6 = from->debug.i386_regs.dr6;
        ctx->Dr7 = from->debug.i386_regs.dr7;
    }
    if ((from->flags & SERVER_CTX_EXTENDED_REGISTERS) && (to_flags & CONTEXT_I386_EXTENDED_REGISTERS))
    {
        ctx->ContextFlags |= CONTEXT_I386_EXTENDED_REGISTERS;
        memcpy( ctx->ExtendedRegisters, from->ext.i386_regs, sizeof(ctx->ExtendedRegisters) );
    }
    if ((from->flags & SERVER_CTX_YMM_REGISTERS) && (to_flags & CONTEXT_I386_XSTATE))
    {
        CONTEXT_EX *xctx = (CONTEXT_EX *)(to + 1);
        XSTATE *xs = (XSTATE *)((char *)xctx + xctx->XState.Offset);

        xs->Mask &= ~4;
        if (user_shared_data->XState.CompactionEnabled) xs->CompactionMask = 0x8000000000000004;
        for (i = 0; i < ARRAY_SIZE( from->ymm.regs.ymm_high); i++)
        {
            if (!from->ymm.regs.ymm_high[i].low && !from->ymm.regs.ymm_high[i].high) continue;
            memcpy( &xs->YmmContext, &from->ymm.regs, sizeof(xs->YmmContext) );
            xs->Mask |= 4;
            break;
        }
    }
#endif
    return STATUS_SUCCESS;
}


/**********************************************************************
 *           BTCpuSetContext  (box86cpu.@)
 */
NTSTATUS WINAPI BTCpuSetContext( HANDLE thread, HANDLE process, void *unknown, I386_CONTEXT *ctx )
{
    WOW64_CPURESERVED *cpu = NtCurrentTeb()->TlsSlots[WOW64_TLS_CPURESERVED];
    x86emu_t *emu = NtCurrentTeb()->TlsSlots[0];
    DWORD flags = ctx->ContextFlags & ~CONTEXT_i386;

    if (flags & CONTEXT_I386_CONTROL)
    {
        R_EBP = ctx->Ebp;
        R_ESP = ctx->Esp;
        R_EIP = ctx->Eip;
        R_CS  = ctx->SegCs;
        R_SS  = ctx->SegSs;
        emu->eflags.x32 = ctx->EFlags;
        cpu->Flags |= WOW64_CPURESERVED_FLAG_RESET_STATE;
    }
    if (flags & CONTEXT_I386_INTEGER)
    {
        R_EAX = ctx->Eax;
        R_EBX = ctx->Ebx;
        R_ECX = ctx->Ecx;
        R_EDX = ctx->Edx;
        R_ESI = ctx->Esi;
        R_EDI = ctx->Edi;
    }
    if (flags & CONTEXT_I386_SEGMENTS)
    {
        R_DS = ctx->SegDs;
        R_ES = ctx->SegEs;
        R_FS = ctx->SegFs;
        R_GS = ctx->SegGs;
    }
#if 0  /* FIXME */
    if (flags & CONTEXT_I386_FLOATING_POINT)
    {
        to->flags |= SERVER_CTX_FLOATING_POINT;
        to->fp.i386_regs.ctrl     = ctx->FloatSave.ControlWord;
        to->fp.i386_regs.status   = ctx->FloatSave.StatusWord;
        to->fp.i386_regs.tag      = ctx->FloatSave.TagWord;
        to->fp.i386_regs.err_off  = ctx->FloatSave.ErrorOffset;
        to->fp.i386_regs.err_sel  = ctx->FloatSave.ErrorSelector;
        to->fp.i386_regs.data_off = ctx->FloatSave.DataOffset;
        to->fp.i386_regs.data_sel = ctx->FloatSave.DataSelector;
        to->fp.i386_regs.cr0npx   = ctx->FloatSave.Cr0NpxState;
        memcpy( to->fp.i386_regs.regs, ctx->FloatSave.RegisterArea, sizeof(to->fp.i386_regs.regs) );
    }
    if (flags & CONTEXT_I386_DEBUG_REGISTERS)
    {
        to->flags |= SERVER_CTX_DEBUG_REGISTERS;
        to->debug.i386_regs.dr0 = ctx->Dr0;
        to->debug.i386_regs.dr1 = ctx->Dr1;
        to->debug.i386_regs.dr2 = ctx->Dr2;
        to->debug.i386_regs.dr3 = ctx->Dr3;
        to->debug.i386_regs.dr6 = ctx->Dr6;
        to->debug.i386_regs.dr7 = ctx->Dr7;
    }
    if (flags & CONTEXT_I386_EXTENDED_REGISTERS)
    {
        to->flags |= SERVER_CTX_EXTENDED_REGISTERS;
        memcpy( to->ext.i386_regs, ctx->ExtendedRegisters, sizeof(to->ext.i386_regs) );
    }
    if (flags & CONTEXT_I386_XSTATE)
    {
        const CONTEXT_EX *xctx = (const CONTEXT_EX *)(from + 1);
        const XSTATE *xs = (const XSTATE *)((const char *)xctx + xctx->XState.Offset);

        to->flags |= SERVER_CTX_YMM_REGISTERS;
        if (xs->Mask & 4) memcpy( &to->ymm.regs.ymm_high, &xs->YmmContext, sizeof(xs->YmmContext) );
    }
#endif
    return NtSetInformationThread( thread, ThreadWow64Context, ctx, sizeof(*ctx) );
}


/**********************************************************************
 *           BTCpuResetToConsistentState  (box86cpu.@)
 */
NTSTATUS WINAPI BTCpuResetToConsistentState( EXCEPTION_POINTERS *ptrs )
{
    ERR( "not implemented\n" );
    return STATUS_SUCCESS;
}


/**********************************************************************
 *           BTCpuTurboThunkControl  (box86cpu.@)
 */
NTSTATUS WINAPI BTCpuTurboThunkControl( ULONG enable )
{
    if (enable) return STATUS_NOT_SUPPORTED;
    /* we don't have turbo thunks yet */
    return STATUS_SUCCESS;
}
